var init_mobile_menu = function () {
    var $mobile_switch = $(".toc.item");
    var $mobile_menu = $("#mobile-menu");

    $mobile_switch.on("click", function () {
        $mobile_menu.toggle();
    });
}

var init_video_player = function (dom_str) {
    $(dom_str).embed();
}
